This repository contains configuration files from different CI/CD vendors, based on the [.gitlab-ci.yml](https://gitlab.com/treagitlab/tr_green_pipeline/-/blob/master/.gitlab-ci.yml). Currently the .gitlab-ci.yml has been converted to configuration files from 4 CI platforms:
 - [Jenkins: Jenkinsfile](Jenkinsfile)
 - [Travis: .travis.yml](.travis.yml)
 - [CircleCI: .circleci/config.xml](.circleci/config.xml)
 - [Github Actions: .github/workflows/main.yml](.github/workflows/main.yml)